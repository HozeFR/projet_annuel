#!/bin/bash
sudo yum install python3 -y
sudo yum install curl -y
sudo chmod +x /usr/local/bin/docker-compose
cd projet_annuel/  
sudo usermod -a -G docker $USER
sudo chown $USER /var/run/docker.sock
docker-compose up -d

